package com.jhorwitz.trivia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.jhorwitz.trivia.entity.Player;

public class InsertPlayer {
	
	static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("trivia");
	static EntityManager em = entityManagerFactory.createEntityManager();	
	static EntityTransaction userTransaction = em.getTransaction();

	
	
	public static void createPlayer(long id, String name, int score, String team, String username){
		userTransaction.begin();
		Player player = new Player();
		player.setId(id);
		player.setName(name);
		player.setScore(score);
		player.setTeam(team);
		player.setUsername(username);
		em.persist(player);
		userTransaction.commit();
	}
	
	public static void main(String[] args) {						
		InsertPlayer.createPlayer(5, "jimmy", 99, "green", "littlejimmy");
		System.out.println(InsertPlayer.getPlayer().toString());
	}
	
	@SuppressWarnings("unchecked")
	public static Player getPlayer() {
		Query query = em.createQuery("SELECT x FROM players x");
		List<Player> list = query.getResultList();
		Player p = new Player();
		for(int i = 0; i < list.size(); i++){
			p = list.get(i);
		}
		return p;
	}

}
